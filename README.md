# Generalised HCT and FVQ Interpolation Functions

* Mathematica codes to generate, test and plot the generalised HCT and FVQ interpolation functions.

Three folders: 
* Mathematica - the files to generate matlab code (folder Defs has the general definitions and for each file there is a Wolfram language script, .wls, and a pdf version);
* Matlab - the generated THCT routines and sample codes to use them;
* Svgs - the raw svgs of the functions in the minimal sets.

The Mathematica notebooks are available upon request.
