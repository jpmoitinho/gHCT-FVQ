% set geometry of the element
% x=[0,1.3,0.8];
% y=[0,0.2,0.5];
% x=[0,1.3,1.3];
% y=[0,0.2,0.3];
x=[0,1,0.5];
y=[0,0,sqrt(3)/2];


r = input("Order of continuity (1 to 3) = ");
mind = [ 3, 7, 9];
maxd = [ 12, 12, 12];

d = input(sprintf("Degree (%d to %d) = ", mind(r), maxd(r)));

T = eval(sprintf("THCTr%dd%d(x,y)", r, d));

desl = input(sprintf("Displacement to plot (1 to %d) = ", size(T,2)));

cs = input("Surface (0) or Contour (!=0)? ");

npts = 1001;
deltas = linspace(0,1,npts);

% The area coordinates npts x npts in the master element
L2 = repmat(deltas,[npts,1]).*deltas';
L3 = repmat((1-deltas)',[1,npts]);
L1 = ones(npts)-L2-L3;

% The area coordinates of each primitive in the master element
Li = cell(3,3);

Li{1,1} = L1/3;
Li{1,2} = L1/3 + L2;
Li{1,3} = L1/3 + L3;

Li{2,1} = L1/3 + L3;
Li{2,2} = L1/3;
Li{2,3} = L1/3 + L2;

Li{3,1} = L1/3 + L2;
Li{3,2} = L1/3 + L3;
Li{3,3} = L1/3;

% The coordinates of the points in each primitive element
xx = cell(1,3);
yy = cell(1,3);
for p=1:3
    xx{p} = Li{p,1}*x(1)+Li{p,2}*x(2)+Li{p,3}*x(3);
    yy{p} = Li{p,1}*y(1)+Li{p,2}*y(2)+Li{p,3}*y(3);
end

% Set the solution to represent
sol = zeros(size(T,2),1); sol(desl) = 1;
% sol = rand(size(T,2),1); % also works...

soli = cell(1,3);
for p = 1:3
    % The global basis in primitive p as a matrix
    Base = reshape(DefineBasis(d, Li{p,1}, Li{p,2}, Li{p,3}), npts*npts, []);
    % The values of the solution at the points
    soli{p} = reshape(Base*T(:,:,p)*sol,npts,npts);
end

%  Draw the surface
clf
shading interp
hold on
axis equal

if (cs ==0)
    maxf = max([soli{1}(:);soli{2}(:);soli{3}(:)]);
    minf = min([soli{1}(:);soli{2}(:);soli{3}(:)]);
    dx = max(x)-min(x);
    dy = max(y)-max(y);
    ratio = (maxf-minf)/max(dx,dy);
    for p = 1:3
        h = surf(xx{p},yy{p},soli{p}); 
        set(h,'edgecolor','none');
    end
    set(gca,'DataAspectRatio',[1 1 ratio])
else
    ms = max([abs(soli{1}(:));abs(soli{2}(:));abs(soli{3}(:))]);
    c = sort([linspace(-ms,ms,21),-1e-14,1e-14]);
    %     c = linspace(-ms,ms,21);
    for p = 1:3
        h = contour(xx{p},yy{p},soli{p},c);
    end
end
hold off
