% Coordinates of the element
x=[0, 1, 0.5];
y=[0, 0, sqrt(3)/2];
% The transformation basis for this element with dims (21,33,3)
% 1 is the order of continuity and 5 is the degree
T = THCTr1d5(x, y);
% Choose the centre, where all the functions are defined and equal
L1 = 1/3; L2 = L1; L3 = L1;
% The basis at a point with dims (1,21)
Basis = DefineBasis(5, L1,L2,L3);
% For printing
fmt1 = '\nPrimitive %d at point (%f, %f, %f)\n';
fmt2 = [ repmat('%8.5f ', 1, 10),'\n'];
% The functions at that point for each primitive element
for p=1:3
    fprintf(fmt1, p, L1, L2, L3);
    fprintf(fmt2, Basis*T(:,:,p)); fprintf('\n'); % each with dimension (1,33)
end
% A point inside primitive element 1
L1 = 1/8; L2 = 7/16; L3 = L2; p = 1;
Basis = DefineBasis(5, L1,L2,L3);
fprintf(fmt1, p, L1, L2, L3);
fprintf(fmt2, Basis*T(:,:,p)); fprintf('\n'); 
