% set geometry of the element
x=[0,4,2,0];
y=[0,0,1,1];

x=[0,2,2.3,-0.5];
y=[0,-.4,1.3,0.5];

aux = (x(2)-x(4))*(y(1)-y(3))-(x(1)-x(3))*(y(2)-y(4));
xc = -( ...
    x(4)*(x(3)*(y(1)-y(2)) + x(1)*(y(2)-y(3))) + ...
    x(2)*(x(1)*(y(3)-y(4)) + x(3)*(y(4)-y(1))) ...
    ) / aux;
yc = ( ...
    y(2)*(y(1)*(x(3)-x(1)) + y(3)*(x(4)-x(1))) + ...
    y(4)*(y(1)*(x(2)-x(3)) + y(3)*(x(1)-x(2))) ...
    ) / aux;

% move origin and add centre...
x = [ x-xc 0 ];
y = [ y-yc 0 ];

C1 = sqrt(x(1)^2+y(1)^2); C2 = sqrt(x(2)^2+y(2)^2); 
C3 = sqrt(x(3)^2+y(3)^2); C4 = sqrt(x(4)^2+y(4)^2); 

% The verts of each primitive
primverts = [5 1 2; 5 2 3; 5 3 4; 5 4 1];

r = input("Order of continuity (1 to 2) = ");
mind = [ 3 7 ];
maxd = [ 5 8 ];

d = input(sprintf("Degree (%d to %d) = ", mind(r), maxd(r)));

T = eval(sprintf("TFVQr%dd%d(x,y)", r, d));

desl = input(sprintf("Displacement to plot (1 to %d) = ", size(T,2)));

cs = input("Surface (0) or Contour (!=0)? ");

npts = 501;
deltas = linspace(0,1,npts);

% The area coordinates npts x npts in the generic element (
L2 = repmat(deltas,[npts,1]).*deltas';
L3 = repmat((1-deltas)',[1,npts]);
L1 = ones(npts)-L2-L3;

% The area coordinates of primitive 1 extended to each primitive
% Mapped according to FVQLsToLis

Li = cell(4,3);

Li{1,1} = L1;
Li{1,2} = L2;
Li{1,3} = L3;

Li{2,1} = 1 - L2 + L3*C3/C1;
Li{2,2} = -L3*C3/C1;
Li{2,3} = L2;

Li{3,1} = 1 + L2*C3/C1 + L3*C4/C2;
Li{3,2} = -L2*C3/C1;
Li{3,3} = -L3*C4/C2;

Li{4,1} = 1 + L2*C4/C2 - L3;
Li{4,2} = L3;
Li{4,3} = -L2*C4/C2;

% The coordinates of the points in each primitive element
xx = cell(1,4);
yy = cell(1,4);
for p=1:4
    xx{p} = L1*x(primverts(p,1))+L2*x(primverts(p,2))+L3*x(primverts(p,3));
    yy{p} = L1*y(primverts(p,1))+L2*y(primverts(p,2))+L3*y(primverts(p,3));
end

% Set the solution to represent
sol = zeros(size(T,2),1); sol(desl) = 1;
% sol = rand(size(T,2),1); % also works...

soli = cell(1,4);
for p = 1:4
    % The global basis in primitive p as a matrix
    Base = reshape(DefineBasis(d, Li{p,1}, Li{p,2}, Li{p,3}), npts*npts, []);
    % The values of the solution at the points
    soli{p} = reshape(Base*T(:,:,p)*sol,npts,npts);
end

%  Draw the surface
clf
shading interp
hold on
axis equal

if (cs ==0)
    maxf = max([soli{1}(:);soli{2}(:);soli{3}(:);soli{4}(:)]);
    minf = min([soli{1}(:);soli{2}(:);soli{3}(:);soli{4}(:)]);
    dx = max(x)-min(x);
    dy = max(y)-max(y);
    ratio = (maxf-minf)/max(dx,dy);
    for p = 1:4
        h = surf(xx{p},yy{p},soli{p}); 
        set(h,'edgecolor','none');
    end
    set(gca,'DataAspectRatio',[1 1 ratio])
else
    ms = max([abs(soli{1}(:));abs(soli{2}(:));abs(soli{3}(:));abs(soli{4}(:))]);
    c = sort([linspace(-ms,ms,11),0]);
    %     c = linspace(-ms,ms,21);
    for p = 1:4
        h = contour(xx{p},yy{p},soli{p},c);
    end
    ii=[1,2,3,4,1];
    plot(x(ii),y(ii));
end
hold off
